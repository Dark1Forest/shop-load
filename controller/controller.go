package controller

import (
	"shop-load/server"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ApiController struct {
}


func Controller(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": server.Server()})
	return
}