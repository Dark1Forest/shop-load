package routers

import (
	"github.com/gin-gonic/gin"
	"shop-load/controller"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/hello", controller.Controller)
	return r
}